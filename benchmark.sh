#!/usr/bin/env bash

# Remove '\r from script
# "sed -i 's/\r$//' filename"

currentDir=$(pwd)
precision=$1
benchmarkFilePath=${currentDir}/benchmarkResults_${precision}_precision.txt
jarName="e-calculator.jar"

java -jar ${jarName} -t 1 > ${benchmarkFilePath}

for i in {2..32..2}
do
   java -jar ${jarName} -t ${i} >> ${benchmarkFilePath}
done