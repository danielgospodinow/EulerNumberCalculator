package com.boryananacheva.project.ecalculator;

import com.boryananacheva.project.ecalculator.utils.Constants;
import com.boryananacheva.project.ecalculator.utils.EulerSumMember;
import org.apfloat.Apfloat;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class EulerCalculator {

    public static Apfloat calculate(long members, int totalThreads, int granularity) {
        List<List<EulerSumMember>> batches = getBatches(members, granularity, totalThreads);
        ExecutorService executor = Executors.newFixedThreadPool(totalThreads);
        Apfloat[] results = new Apfloat[batches.size()];

        Instant start = Instant.now();

        Stream.iterate(0, n -> n + 1)
                .limit(batches.size())
                .forEach(batchIndex -> executor.submit(() -> results[batchIndex] = getBatchSum(batches.get(batchIndex))));

        awaitExecutorTermination(executor);

        Instant finish = Instant.now();
        long duration = Duration.between(start, finish).toMillis();
        System.out.println("Time: " + String.format("%.3f", duration / (float) 1000));

        return Arrays.stream(results)
                .reduce(new Apfloat(0, Constants.FLOAT_PRECISION), Apfloat::add);
    }

    private EulerCalculator() {
        throw new RuntimeException("This shouldn't be instantiated!");
    }

    private static Apfloat getBatchSum(List<EulerSumMember> batch) {
        return batch.stream()
                .map(EulerSumMember::getValue)
                .reduce(new Apfloat(0, Constants.FLOAT_PRECISION), Apfloat::add);
    }

    private static List<List<EulerSumMember>> getBatches(long members, int granularity, int totalThreads) {
        int totalBatches = granularity * totalThreads;
        List<List<EulerSumMember>> batches = new ArrayList<>(totalBatches);

        for (int i = 0; i < totalBatches; ++i) {
            batches.add(new ArrayList<>());
        }

        for (int i = 0; i < members / 2; ++i) {
            batches.get(i % totalBatches).add(new EulerSumMember(i));
            batches.get(i % totalBatches).add(new EulerSumMember((members - 1) - i));
        }

        return batches;
    }

    private static void awaitExecutorTermination(ExecutorService workersSpawner) {
        workersSpawner.shutdown();
        try {
            if (!workersSpawner.awaitTermination(30, TimeUnit.MINUTES)) {
                workersSpawner.shutdownNow();
            }
        } catch (InterruptedException e) {
            workersSpawner.shutdownNow();
            System.out.println("Image generation was interrupted!");
            e.printStackTrace();
        }
    }
}
