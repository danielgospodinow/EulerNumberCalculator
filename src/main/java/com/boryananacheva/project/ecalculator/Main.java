package com.boryananacheva.project.ecalculator;

import com.boryananacheva.project.ecalculator.utils.EulerNumberCommandOptions;
import org.apache.commons.cli.*;
import org.apfloat.Apfloat;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main {

    public static void main(String[] args) {
        EulerNumberCommandOptions options = getOptions(args);

        Apfloat eulerNumber = EulerCalculator.calculate(
                options.getPrecision(),
                options.getNumOfThreads(),
                options.getGranularity());

        writeToFile(options.getOutputFileName(), eulerNumber.toString());

        if(!options.isQuiteMode()) {
            System.out.println("Output: " + eulerNumber.toString());
        }
    }

    private static EulerNumberCommandOptions getOptions(String[] args) {
        CommandLineParser commandLineParser = new DefaultParser();

        Options options = new Options();

        Option precision = new Option("p", "precision", true, "precision level");
        Option threads = new Option("t", "tasks", true, "maximum threads in the process");
        Option outputImageName = new Option("o", "output", true, "name of the output image");
        Option quietMode = new Option("q", "quiet", false, "omits the debug messages");
        Option granularity = new Option("g", "granularity", true, "granularity level");

        options.addOption(precision);
        options.addOption(threads);
        options.addOption(outputImageName);
        options.addOption(quietMode);
        options.addOption(granularity);

        CommandLine commandLine = null;
        try {
            commandLine = commandLineParser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Failed to parse command line arguments!");
            System.exit(1);
        }

        return new EulerNumberCommandOptions(
                commandLine.getOptionValue("p"),
                commandLine.getOptionValue("t"),
                commandLine.getOptionValue("o"),
                commandLine.hasOption("q"),
                commandLine.getOptionValue("g"));
    }

    private static void writeToFile(String filename, String message) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8))) {
            writer.write(message);
        } catch (IOException e) {
            System.out.println("Failed to write result to file!");
        }
    }
}
