package com.boryananacheva.project.ecalculator.utils;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

public class EulerSumMember {

    private long memberNumber;
    private Apfloat value;

    public EulerSumMember(long n) {
        this.memberNumber = n;
    }

    public Apfloat getValue() {
        if(value == null) {
            Apfloat numerator = getApfloat(3).subtract(getApfloat(4).multiply(ApfloatMath.pow(getApfloat(memberNumber), 2)));
            Apfloat denominator = factorial(getApfloat(2).multiply(getApfloat(memberNumber)).add(getApfloat(1)));

            value = numerator.divide(denominator);
        }

        return value;
    }

    private static Apfloat factorial(Apfloat number) {
        return Factorial.calculate(number.longValue());
    }

    private static Apfloat getApfloat(long n) {
        return new Apfloat(n, Constants.FLOAT_PRECISION);
    }
}
