package com.boryananacheva.project.ecalculator.utils;

import org.apfloat.ApfloatContext;

public class EulerNumberCommandOptions {

    private static final long DEFAULT_PRECISION = 2000;
    private static final int DEFAULT_NUM_OF_THREADS = 1;
    private static final String DEFAULT_OUTPUT_FILE_NAME = "result.txt";
    private static final int DEFAULT_GRANULARITY = 4;

    private long precision;
    private int numOfThreads;
    private String outputFileName;
    private boolean quiteMode;
    private int granularity;

    public EulerNumberCommandOptions(String precision, String numOfThreads, String outputFileName, boolean quietMode, String granularity) {
        this.precision = (precision != null) ? Long.parseLong(precision) : DEFAULT_PRECISION;
        this.numOfThreads = (numOfThreads != null) ? Integer.parseInt(numOfThreads) : DEFAULT_NUM_OF_THREADS;
        this.outputFileName = (outputFileName != null) ? outputFileName : DEFAULT_OUTPUT_FILE_NAME;
        this.quiteMode = quietMode;
        this.granularity = (granularity != null) ? Integer.parseInt(granularity) : DEFAULT_GRANULARITY;

        ApfloatContext.getContext().setNumberOfProcessors(this.numOfThreads);
    }

    public long getPrecision() {
        return precision;
    }

    public int getNumOfThreads() {
        return numOfThreads;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public boolean isQuiteMode() {
        return quiteMode;
    }

    public int getGranularity() {
        return granularity;
    }
}
