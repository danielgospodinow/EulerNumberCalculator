package com.boryananacheva.project.ecalculator.utils;

import org.apfloat.Apfloat;

import java.util.stream.Stream;

public class Factorial {

    public static Apfloat calculate(long n) {
        return Stream.iterate(1, i -> i + 1)
                .limit(n)
                .map(Apfloat::new)
                .reduce(new Apfloat(1, Constants.FLOAT_PRECISION), Apfloat::multiply);
    }

    private Factorial() {
        throw new RuntimeException("This should not be instantiated!");
    }
}
