package com.boryananacheva.project.ecalculator.utils;

public class Constants {

    public static final int FLOAT_PRECISION = 128;

    private Constants() {
        throw new RuntimeException("This shouldn't be instantiated!");
    }
}
